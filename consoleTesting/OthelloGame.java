import java.util.*;

public class OthelloGame 
{
	boolean gameOver;
	GameBoard board;
	Scanner scan;
	
	char turn;
	char prev;
	
	char winner;

    public OthelloGame() 
    {
    	gameOver = false;
    	  	
    	turn = 'R';
    	prev = 'B';
    	winner = ' ';
    	
    	scan = new Scanner(System.in);
    }
    
    public void run()
    {
    	int alphaBetaBound = 5;
    	int turnsWithoutAction = 0;
    	
    	board = new GameBoard();
    	winner = ' ';    	
    	
    	while(gameOver == false)
    	{
    		System.out.println(board.toString());
 
 			if(turnsWithoutAction >= 2)
 				gameOver = true;
 
 			if(turn == 'R')
 			{	
    			//move(turn, prev);
    			
    				System.out.println();
    			Move bestMove = randomMove(turn, prev, alphaBetaBound, alphaBetaBound, board);
    			if(bestMove.getRow() >=0 && bestMove.getCol() >= 0)
    			{	
    				board.applyMove(bestMove.getRow(), bestMove.getCol(), turn, prev);
    				turnsWithoutAction = 0;
    			}	
    			else
    			{
    				turnsWithoutAction++;
    				System.out.println("R didn't move");
    			}
    				
 			}	
    		else if(turn == 'B')
    		{	
    			System.out.println();
    			Move bestMove = aiMove(turn, prev, alphaBetaBound, alphaBetaBound, board);
    			if(bestMove.getRow() >=0 && bestMove.getCol() >= 0)	
    			{	
    				System.out.println("Game State = " + bestMove.getUtility());
    				board.applyMove(bestMove.getRow(), bestMove.getCol(), turn, prev);
    				turnsWithoutAction = 0;
    			}	
    			else
    			{
    				turnsWithoutAction++;
    				System.out.println("B didn't move");
    			}	
    		}
    		if(turn == 'R')
    		{	
    			turn = 'B';
    			prev = 'R';
    		}	
    		else if(turn == 'B')
    		{
     			turn = 'R';	
     			prev = 'B';
    		}			
    		

    
    	}
    	
    	    if(board.getTokensB() + board.getTokensR() >= board.getBoardSize() * board.getBoardSize())
    		{
    			if(board.getTokensB() > board.getTokensR())
    			{
    				winner = 'B';
    			}
    			else if(board.getTokensB() < board.getTokensR())
    			{
    				winner = 'R';
    			}
    				
    			gameOver = true;		
    		}
    	
    	System.out.println("GAME OVER! " + winner + " wins!!!");
    	System.out.println("B = " + board.getTokensB() + "     R = " + board.getTokensR());
    }
    
    private void move(char player, char other)
    {
    	System.out.println(player + "'s turn...");
    	System.out.println("Move exists? " + board.moveExists(player,other));
    	
    	boolean moveValid = false;
    	
		if(!board.moveExists(player,other))
			return;
		
    	
    	while(!(moveValid == true))
    	{
	    	System.out.println("Select a move!");	
	    	String input = scan.nextLine();
	    	System.out.println();
	    	
	    	String[] coordinateStrings = input.split(",");
	    	
	    	int row = Integer.parseInt(coordinateStrings[0]);
	    	int col = Integer.parseInt(coordinateStrings[1]);
	    	
	    	if(board.isValidMove(row,col,player, other))
	    	{
	    		board.applyMove(row,col,player, other);
	    		moveValid = true;
	    	}
	    	
    	}	
    }
    
    private Move randomMove(char player, char other, int maxLevel, int level, GameBoard currentBoard)
    {
    	if(currentBoard.numValidMoves(player,other) > 0)
    		return currentBoard.getValidMoves(player,other,level,maxLevel).get((int)(Math.random() * currentBoard.getValidMoves(player,other,level,maxLevel).size()));
    		
    		return new Move(-1,-1,player,other,currentBoard.heuristicFunction(), level, maxLevel);
    }
    
    private Move aiMove(char player, char other, int maxLevel, int level, GameBoard currentBoard)
    {	
    
    	
    	if(currentBoard.numValidMoves(player,other) <= 0)
    	{
    		return new Move(-1,-1,player,other,currentBoard.heuristicFunction(), level, maxLevel);
    	}

    	
    	if(level > 1 && (maxLevel-level)%2 == 1)
    	{
    		Move bestMove = new Move(-1,-1,player,other,currentBoard.heuristicFunction(), level, maxLevel);;
    		
    		double high = Integer.MIN_VALUE;
    		
    		for(Move testMove : currentBoard.getValidMoves(player,other,level, maxLevel))
    		{
    			GameBoard test = new GameBoard();
    			test.setPositions(currentBoard.getPositions());  		
    			test.applyMove(testMove.getRow(), testMove.getCol(), testMove.getPlayer(), testMove.getOther());
    			
    			double utility = aiMove(other,player,maxLevel, level - 1, test).getUtility();
    			
    			if(high < utility)
    			{
    				high = utility;
    				bestMove = testMove;
    			}	
    		}	
    		
			
    		
    		return bestMove;
    		
    	}
    	else if(level > 1 && (maxLevel-level)%2 == 0)
    	{
    		Move bestMove = new Move(-1,-1,player,other,currentBoard.heuristicFunction(), level, maxLevel);
    		
    		double low = Integer.MAX_VALUE;
    		
    		for(Move testMove : currentBoard.getValidMoves(player,other,level, maxLevel))
    		{
    			GameBoard test = new GameBoard();
    			test.setPositions(currentBoard.getPositions());  		
    			test.applyMove(testMove.getRow(), testMove.getCol(), testMove.getPlayer(), testMove.getOther());
    			
    			double utility = aiMove(other,player,maxLevel, level - 1, test).getUtility();
    			
    			 if(low > utility)
    			{
    				low = testMove.utility;
    				bestMove = testMove;
    			}
    		}	
    			
    			
    		return bestMove;	
    	}
    	else
    	{
    		
    		Move bestMove = new Move(-1,-1,player,other,currentBoard.heuristicFunction(), level, maxLevel);
    		return bestMove;	
    	}
    	
    			
    }
    
    	
    
   
    
}