/**
 * @(#)OthelloGameRunner.java
 *
 *
 * @author 
 * @version 1.00 2016/11/30
 */


public class OthelloGameRunner 
{
    public static void main(String args[])
    {
    	OthelloGame game = new OthelloGame();
    	game.run();	
    }  
}