import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import javax.swing.JOptionPane;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import java.util.StringTokenizer;

public class OthelloService
{
	public static void main(String[] args)
	{
	    // default port
	    int port = 8000;
	    
	    // parse command line arguments to override defaults
	    if (args.length > 0)
		{
		    try
			{
			    port = Integer.parseInt(args[0]);
			    
			}
		    catch (NumberFormatException ex)
			{
			    System.err.println("USAGE: java OthelloService [port]");
			    System.exit(1);
			}
		}
	    
	    // set up an HTTP server to listen on the selected port
	    try
		{
		    InetSocketAddress addr = new InetSocketAddress(port);
		    HttpServer server = HttpServer.create(addr, 1);
		    
		    server.createContext("/move.html", new MoveHandler());
        
		    server.start();
		}
	    catch (IOException ex)
		{
		    ex.printStackTrace(System.err);
		    System.err.println("Could not start server");
		}
	}
    
    /**
     * An HTTP handler for roll requests.
     */
    public static class MoveHandler implements HttpHandler
    {
        @Override
	    public void handle(HttpExchange ex) throws IOException
        {
            // we assume the query encodes tan uncontrolled move, . a controlled move, * a selected
	    // move, and n the current player
	    
        System.err.println(ex.getRequestURI());
        String q = ex.getRequestURI().getQuery();
	    
	    StringBuilder reponse = new StringBuilder();

	    // decode the string
	    StringTokenizer tok = new StringTokenizer(q, ";");
	    if (tok.countTokens() != 3)
		{
		    sendResponse(ex, error(q, "malformed state"));
		}
	    else
		{
		    String spaces = tok.nextToken();
		    System.out.println("SPACES = " + spaces);
		    try
			{
			    char player = tok.nextToken().charAt(0);
			    char other = ' ';
			    int numGoodMoves = 0;
			    
			    int index = Integer.parseInt(tok.nextToken());
			    
			    if(player == 'R')
			    	other = 'B';
			    else if(player == 'B')
			    	other = 'R';	

			    if (player != 'R' && player != 'B')
				{
				    sendResponse(ex, error(q, "invalid player"));
				}
			    else if(player == 'R')
				{
				    GameBoard board = new GameBoard();
				    
				    
				    for (int i = 0; i < spaces.length(); i++)
					{
						
					    if(spaces.charAt(i) == 'X')
					    	board.setSpot(i/8,i%8,' ');	
					    else
					    	board.setSpot(i/8,i%8,spaces.charAt(i));
					    		
					}
					
					numGoodMoves = board.numValidMoves(player,other);
					
				    if (index == -1 && numGoodMoves > 0)
					{
					    sendResponse(ex, error(q, "no spaces selected"));
					}
					else if(index == -1)
					{
						if(player == 'B')
						{
							player = 'R';
							other = 'B';
						}
						else if(player == 'R')
						{
							player = 'B';
							other = 'R';
						}
						
						StringBuilder newState = new StringBuilder(spaces);
						
					
						
						Map<String, String> response = new HashMap<String, String>();
					    response.put("state", q);
					    response.put("spaces", newState.toString());
					    response.put("player", "" + player);
					    response.put("availableMoves", "" + numGoodMoves);
					    
					    sendResponse(ex, response);
					}	
					else if(!board.isValidMove(index/8, index%8, player, other))
					{
						 sendResponse(ex, error(q, "illegal placement"));
					}		
				    else
					{
					    StringBuilder newState = new StringBuilder();
					    
					    
					    board.applyMove(index/8,index%8,player,other);
					    
					    
						for(int i = 0; i < 64; i++)
						{
							if(board.getSpot(i/8,i%8) == ' ')
								newState.append('X');
							else	
								newState.append(board.getSpot(i/8,i%8));
						}
						
					    if(player == 'B')
						{
							player = 'R';
							other = 'B';
						}
						else if(player == 'R')
						{
							player = 'B';
							other = 'R';
						}

					    Map<String, String> response = new HashMap<String, String>();
					    response.put("state", q);
					    response.put("spaces", newState.toString());
					    response.put("player", "" + player);
					    response.put("availableMoves", "" + numGoodMoves);
					    
					    sendResponse(ex, response);
					}
				}
				else if(player == 'B')
				{
					
					GameBoard board = new GameBoard();
					
				    
				    for (int i = 0; i < spaces.length(); i++)
					{
						
					    if(spaces.charAt(i) == 'X')
					    	board.setSpot(i/8,i%8,' ');	
					    else
					    	board.setSpot(i/8,i%8,spaces.charAt(i));
					    		
					}
					
					numGoodMoves = board.numValidMoves(player,other);
					
					if(board.numValidMoves(player,other) > 0)
					{
					Move bestMove = aiTechnology.aiMove(player,other,5,5,board);
					
					StringBuilder newState = new StringBuilder();
					    
					board.applyMove(bestMove.getRow(),bestMove.getCol(),player,other);
					    
					    
						for(int i = 0; i < 64; i++)
						{
							if(board.getSpot(i/8,i%8) == ' ')
								newState.append('X');
							else	
								newState.append(board.getSpot(i/8,i%8));
						}
						
					    if(player == 'B')
						{
							player = 'R';
							other = 'B';
						}
						else if(player == 'R')
						{
							player = 'B';
							other = 'R';
						}

					    Map<String, String> response = new HashMap<String, String>();
					    response.put("state", q);
					    response.put("spaces", newState.toString());
					    response.put("player", "" + player);
					    response.put("availableMoves", "" + numGoodMoves);
					    
					    sendResponse(ex, response);
					} 
					else
					{
						if(player == 'B')
						{
							player = 'R';
							other = 'B';
						}
						else if(player == 'R')
						{
							player = 'B';
							other = 'R';
						}
						
						StringBuilder newState = new StringBuilder(spaces);
						
					
						
						Map<String, String> response = new HashMap<String, String>();
					    response.put("state", q);
					    response.put("spaces", newState.toString());
					    response.put("player", "" + player);
					    response.put("availableMoves", "" + numGoodMoves);
					    
					    sendResponse(ex, response);
					}	
				}	
			}
		    catch (NumberFormatException e)
			{
			    sendResponse(ex, error(q, "malformed player index"));
			}
		}
	}	
    }

    /**
     * An HTTP handler for hint requests.
     */
    
    private static Map<String, String> error(String state, String message)
    {
	Map<String, String> result = new HashMap<String, String>();

	result.put("state", state);
	result.put("message", message);

	return result;
    }
    
    /**
     * Sends a JSON object as a response in the given HTTP exchange.  Each key-value pair
     * in the given map will be copied to the JSON object.
     *
     * @param ex an HTTP exchange
     * @param info a non-empty map
     */
    private static void sendResponse(HttpExchange ex, Map<String, String> info) throws IOException
    {
	// write the response as JSON
	StringBuilder response = new StringBuilder("{");
	for (Map.Entry<String, String> e : info.entrySet())
	    {
		response.append("\"").append(e.getKey()).append("\":")
		    .append("\"").append(e.getValue()).append("\",");
	    }
	response.deleteCharAt(response.length() - 1); // remove last ,
	response.append("}"); // close JSON
	
	ex.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
	byte[] responseBytes = response.toString().getBytes();
	ex.sendResponseHeaders(HttpURLConnection.HTTP_OK, responseBytes.length);
	ex.getResponseBody().write(responseBytes);
	ex.close();
    }
}