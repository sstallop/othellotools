/**
 * @(#)aiTechnology.java
 *
 *
 * @author 
 * @version 1.00 2016/12/14
 */


public class aiTechnology {

    public static Move aiMove(char player, char other, int maxLevel, int level, GameBoard currentBoard)
    {	
    
    	
    	if(currentBoard.numValidMoves(player,other) <= 0)
    	{
    		return new Move(-1,-1,player,other,currentBoard.heuristicFunction(), level, maxLevel);
    	}

    	
    	if(level > 1 && (maxLevel-level)%2 == 1)
    	{
    		Move bestMove = new Move(-1,-1,player,other,currentBoard.heuristicFunction(), level, maxLevel);;
    		
    		double high = Integer.MIN_VALUE;
    		
    		for(Move testMove : currentBoard.getValidMoves(player,other,level, maxLevel))
    		{
    			GameBoard test = new GameBoard();
    			test.setPositions(currentBoard.getPositions());  		
    			test.applyMove(testMove.getRow(), testMove.getCol(), testMove.getPlayer(), testMove.getOther());
    			
    			double utility = aiMove(other,player,maxLevel, level - 1, test).getUtility();
    			
    			if(high < utility)
    			{
    				high = utility;
    				bestMove = testMove;
    			}	
    		}	
    		
			
    		
    		return bestMove;
    		
    	}
    	else if(level > 1 && (maxLevel-level)%2 == 0)
    	{
    		Move bestMove = new Move(-1,-1,player,other,currentBoard.heuristicFunction(), level, maxLevel);
    		
    		double low = Integer.MAX_VALUE;
    		
    		for(Move testMove : currentBoard.getValidMoves(player,other,level, maxLevel))
    		{
    			GameBoard test = new GameBoard();
    			test.setPositions(currentBoard.getPositions());  		
    			test.applyMove(testMove.getRow(), testMove.getCol(), testMove.getPlayer(), testMove.getOther());
    			
    			double utility = aiMove(other,player,maxLevel, level - 1, test).getUtility();
    			
    			 if(low > utility)
    			{
    				low = testMove.utility;
    				bestMove = testMove;
    			}
    		}	
    			
    			
    		return bestMove;	
    	}
    	else
    	{
    		
    		Move bestMove = new Move(-1,-1,player,other,currentBoard.heuristicFunction(), level, maxLevel);
    		return bestMove;	
    	}
    	
    			
    }
    
    
}