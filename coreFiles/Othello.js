function initOthello()
{
    
	makeOthello();
	

    // initialize global variables to keep track of game state
 	selected = 0;// number of kayles currently selected
    remaining = 64; // number of remaining kayle
    staleMoves = 0;
    bTokens = 0;
    rTokens = 0;
    turn = 'B';      // the current player
    realWinner = "GAME IS STILL RUNNING";

    var playerTurn = $("#player")
		.text(" " + turn);

	var winner = 	$("#winner")
		.text(realWinner);
}

function makeOthello()
{
	var display = $("#mainChessBoard");

	for (var i=0; i< 64; i++)
	{

			var a = $("<div/>")
	    	.addClass("space")
	    	.attr("data-selected", "false")
	    	.attr("data-controlled" , "false")
	    	.attr("data-index" , i + "")
	    	.click(selectSpace);

	    	if(i == 27 || i == 36)
	    	{
	    		a.attr("data-controlled" , "R");
	    	}
	    	else if(i == 28 || i == 35)
	    	{
	    		a.attr("data-controlled" , "B");
	    	}
		
	    	display.append(a);
    	
	}	
}

function selectSpace()
{
	var id = parseInt($( this ).attr('data-index'));
	
	if ( $( this ).attr('data-selected') == 'false' && $( this ).attr('data-controlled') == 'false')
	{


		if ( selected == 0)
		{
		    // nothing already selected, so legal
		    $( this ).attr('data-selected', 'true');
		    selected = 1;
		}

	}
	else if ( $( this ).attr('data-selected') == 'true' && $( this ).attr('data-controlled') == 'false')
	{
	    
	    if (selected == 1)
		{
		    // now nothing is selected
	    	$( this ).attr('data-selected', 'false');
		    selected = 0;
		}
	  
	}
}

function bowl()
{

	state = encodeState();
	$.getJSON( "http://localhost:8000/move.html?" + state, makeMove);
	bTokens = 0;
	rTokens = 0;
}

function makeMove(result)
{

	if ("message" in result)
	{
	    $("#message").text(result.message);
	}
	else if(result.availableMoves == "0")
	{
		console.log(turn + " couldn't move");

		if(remaining > 0)
		{
			turn = result.player;

			var playerTurn = $("#player")
			.text(" " + turn);

			
		}

		staleMoves++;
		$(".space").each( function (i) { setState(this, result.spaces.substring(i, i + 1)); } );
		
	}
	else
	{

	    remaining = remaining - selected;

	    selected = 0;
	    
	    // change turn or display winner

		if(remaining > 0)
		{
			turn = result.player;

			  var playerTurn = $("#player")
			.text(" " + turn);
		}

		staleMoves = 0;
		$(".space").each( function (i) { setState(this, result.spaces.substring(i, i + 1)); } );
		
	}



	function setState(elt, code)
	{
	    if (code == 'X')
		{
		    $(elt).attr('data-selected', 'false');
		    $(elt).attr('data-controlled', 'false');
		}
	    else if (code == 'R')
		{
		    $(elt).attr('data-selected', 'false');
		    $(elt).attr('data-controlled', 'R');
		    rTokens++;
		}
	    else if(code == 'B')
		{
		    $(elt).attr('data-selected', 'false');
		    $(elt).attr('data-controlled', 'B');
		    bTokens++;
		}

		if(staleMoves >= 2 || bTokens + rTokens >= 64)
		gameOver();
	}

	
	
}

function gameOver()
{
	if(rTokens > bTokens)
	{
		realWinner = "R IS THE WINNER";
	}
	else if(bTokens > rTokens)
	{
		realWinner = "B IS THE WINNER";
	}
	else
	{
		realWinner = "TIE";
	}

		var winner = 	$("#winner")
		.text(realWinner);
}

function encodeState()
{

	var spaces = "";
	var index = -1;
	$( ".space" ).each( function () 
	{ 
		spaces = spaces + toChar(this); 
		if($(this).attr('data-selected') == 'true')
			index = $(this).attr('data-index');

	} );
	
	state = spaces + ";" + turn + ";" + index;
	console.log("sending new move: " + state);

	return state;

}


function toChar(elt)
{
    if ($( elt ).attr('data-controlled') == 'R')
	{
	    return 'R';
	}
    else if ($( elt).attr('data-controlled') == 'B')
	{
	    return 'B';
	}
    else
	{
	    return 'X';
	}
}

function setState(elt, code)
{
    if (code == 'R')
	{
	    $(elt).attr('data-selected', 'false');
	    $(elt).attr('data-controlled', 'R');
	}
    else if (code == 'B')
	{
	    $(elt).attr('data-selected', 'false');
	    $(elt).attr('data-controlled', 'B');
	}
    else 
	{
	    $(elt).attr('data-selected', 'true');
	    $(elt).attr('data-bowled', 'false');
	}
}

