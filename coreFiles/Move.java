/**
 * @(#)Move.java
 *
 *
 * @author 
 * @version 1.00 2016/12/14
 */


public class Move 
{
	int row;
	int col;
	int level;
	char player;
	char other;
	double utility;
	int maxLevel;


    public Move(int row, int col, char player, char other, double utility, int level, int maxLevel) 
    {
    	this.row = row;
    	this.col = col;
    	this.player= player;
    	this.other = other;
    	this.utility = utility;
    	this.level = level;
    	this.maxLevel = maxLevel;
    }
    
    public int getRow()
    {
    	return row;
    }
    
    public int getCol()
    {
    	return col;
    }
    
    public char getPlayer()
    {
    	return player;
    }
    
    public char getOther()
    {
    	return other;
    }
    
    public double getUtility()
    {
    	return utility;
    }
    
    public int getLevel()
    {
    	return level;
    }
    
    public int getMaxLevel()
    {
    	return maxLevel;
    }
    
    public String toString()
    {
     	return "DEPTH: " + level + " UTILITY: " + utility + "      " + row + "," + col;
    }
    
}