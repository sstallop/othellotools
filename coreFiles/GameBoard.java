import java.util.*; 
import java.util.Arrays;

public class GameBoard 
{
	private char[][] positions;
	private int boardSize;
	private int wTokens;
	private int bTokens;

    public GameBoard() 
    {
    	boardSize = 8;
    	positions = new char[8][8];
    	
    	for(int i = 0; i < boardSize; i++)
    		for(int j = 0; j < boardSize; j++)
    			positions[i][j] = ' ';
    	    	
    	positions[3][3] = 'R';
    	positions[4][3] = 'B';
    	positions[3][4] = 'B';
    	positions[4][4] = 'R';
    	
    	bTokens = 2;
    	wTokens = 2;
    }
    
    
    public String toString()
    {
    	StringJoiner sj = new StringJoiner(System.lineSeparator());
    	
		for (char[] row : positions) 
		{
		    sj.add(Arrays.toString(row));
		}
		
		String result = sj.toString();
		
		return result;
	}
	
	public boolean isValidMove(int row, int col, char player, char other)
	{
		if(positions[row][col] != ' ')
			return false;
		
		if(!isOnBoard(row, col))
			return false;
		
		for(int i = -1; i <= 1; i++)
		{
			for(int j = -1; j <= 1; j++)
			{
				if((i != 0 || j != 0) && isOnBoard(row + i, col + j))
				{
					if(positions[row + i][col + j] == other)
					{						
						boolean searching = true;
						int length = 1;
						
						while(searching)
						{
							length++;
							
							if(!isOnBoard(row+i*length, col+j*length))
								searching = false;
							else if(positions[row+i*length][col+j*length] == ' ')
								searching = false;
							else if(positions[row+i*length][col+j*length] == player)
								return true;
										
						}
							
					}
				}
			}
		}
		return false;
		

			
	
	}
	public void applyMove(int row, int col, char player, char other)
	{
		
		positions[row][col] = player;
		
		if(player == 'B')
			bTokens++;
		else if(player == 'R')
			wTokens++;	
		
		for(int i = -1; i <= 1; i++)
		{
			for(int j = -1; j <= 1; j++)
			{
				if((j != 0 || i != 0) && isOnBoard(row + i, col + j))
				{
					if(positions[row + i][col + j] == other)
					{		
						boolean searching = true;
						int length = 1;
						
							while(searching)
							{
								length++;
								
								if(!isOnBoard(row+i*length, col+j*length))
									searching = false;
								else if(positions[row+i*length][col+j*length] == ' ')
									searching = false;
								else if(positions[row+i*length][col+j*length] == player)
								{
									for(int k = 1; k < length; k++)
									{
										if(positions[row+i*k][col+j*k] == other)
											flipPosition(row+i*k, col+j*k);
									}
								}
											
							}
					}	
				}
			}
		}
	}
	
	public void setSpot(int i, int j, char thing)
	{
		positions[i][j] = thing;
	}
	
	public char getSpot(int i, int j)
	{
		return positions[i][j];
	}
	
	public boolean moveExists(char player, char other)
	{
		
		for(int i = 0; i < positions.length; i++)
    	{
    		for(int j = 0; j < positions[i].length; j++)
    		{
    			if(isValidMove(i,j,player,other))
    				return true;
    		}
    				
    	}
    	
    	return false;
	}
	
	public List<Move> getValidMoves(char player, char other, int level, int maxLevel)
	{
		List<Move> moves = new ArrayList<Move>();
		
		for(int i = 0; i < boardSize; i++)
		{
			for(int j = 0; j < boardSize; j++)
			{
				GameBoard test = new GameBoard();
    			test.setPositions(getPositions());
    				
				if(isValidMove(i,j,player,other))
				{
					test.applyMove(i,j,player,other);
					moves.add(new Move(i,j,player,other, test.heuristicFunction(),level, maxLevel));
				}	
    				
			}
		}
		
		return moves;
	}
	
	public boolean isOnBoard(int row, int col)
	{
		if(row >= 0 && row < boardSize && col >= 0 && col < boardSize)
			return true;
			
		return false;	
	}
	
	private void flipPosition(int row, int col)
	{
		if(isOnBoard(row,col))
		{
			if(positions[row][col] == 'R')
			{
				positions[row][col] = 'B';
				wTokens--;
				bTokens++;
				
			}
			else if(positions[row][col] == 'B')
			{
				positions[row][col] = 'R';
				bTokens--;
				wTokens++;
			}
					
		}		
	}
	
	public Move getBestMove(char player, char other, int level, int maxLevel)
	{
		int bestRow = -1;
    	int bestCol = -1;
    	double maxUtility = Integer.MIN_VALUE;
    	
    	if(!moveExists(player,other))
    	{
    			return new Move(-1,-1,player,other,1000, level, maxLevel); 		
    	}	
			
    	for(Move move : getValidMoves(player, other, level, maxLevel))
    	{
    				int i = move.getRow();
    				int j = move.getCol();
    				
    				GameBoard test = new GameBoard();
    				test.setPositions(getPositions());
    				
    				test.applyMove(i,j,player,other);
    				
    				
    				double value = test.heuristicFunction();
    				//System.out.println(i + "," + j + " VALUE IS " + value);
    				
    				if(maxUtility < value)
    				{
    					maxUtility = value;
    					bestRow = i;
    					bestCol = j;
    				}
    				
    	}		
    		
    	
    	
    	return new Move(bestRow, bestCol, player, other, maxUtility, level, maxLevel);
		
	}
	
	public double heuristicFunction()  
	{
	
		//Piece Advantage
		char player = 'R';
		char other = 'B';
	
		int[] X1 = {-1, -1, 0, 1, 1, 1, 0, -1};
		int[] Y1 = {0, 1, 1, 1, 0, -1, -1, -1};
		
		int[][] heuristicBoard = new int[][]{	
		{20, -3, 11, 8, 8, 11, -3, 20},
		{-3, -7, -4, 1, 1, -4, -7, -3},
		{11, -4, 2, 2, 2, 2, -4, 11},
		{8, 1, 2, -3, -3, 2, 1, 8},
		{8, 1, 2, -3, -3, 2, 1, 8},
		{11, -4, 2, 2, 2, 2, -4, 11},
		{-3, -7, -4, 1, 1, -4, -7, -3},
		{20, -3, 11, 8, 8, 11, -3, 20}};
		
		int PTiles = 0;
		int OTiles = 0;
		int PFrontTiles = 0;
		int OFrontTiles = 0;
		
		double d = 0;
		double p = 0;
		double f = 0;
		
		
		for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				if(positions[i][j] == player)
				{
					d += heuristicBoard[i][j];
					PTiles++;
				}
				else if(positions[i][j] == other)
				{
					d -= heuristicBoard[i][j];
					OTiles++;
				}
				
				if(positions[i][j] != ' ')   {
				for(int k=0; k<8; k++)  
				{
					int x = i + X1[k]; 
					int	y = j + Y1[k];
					
					if(x >= 0 && x < 8 && y >= 0 && y < 8 && positions[x][y] == ' ') 
					{
						if(positions[i][j] == player)  
							PFrontTiles++;
						else if(positions[i][j] == other)
							OFrontTiles++;
						break;
					}
				}
			}
						
			}
					
		}
		
	if(PTiles > OTiles)
		p = (100.0 * PTiles)/(PTiles + OTiles);
	else if(PTiles < OTiles)
		p = -(100.0 * OTiles)/(PTiles + OTiles);
	else p = 0;

	if(PFrontTiles > OFrontTiles)
		f = -(100.0 * PFrontTiles)/(PFrontTiles + OFrontTiles);
	else if(PFrontTiles < OFrontTiles)
		f = (100.0 * OFrontTiles)/(PFrontTiles + OFrontTiles);
	else f = 0;
	
	//Corner Advantage
	
	int PCTiles = 0;
	int OCTiles = 0;
	double c = 0;
	
	if(positions[0][0] == player) PCTiles++;
	else if(positions[0][0] == other) OCTiles++;
	if(positions[0][7] == player) PCTiles++;
	else if(positions[0][7] == other) OCTiles++;
	if(positions[7][0] == player) PCTiles++;
	else if(positions[7][0] == other) OCTiles++;
	if(positions[7][7] == player) PCTiles++;
	else if(positions[7][7] == other) OCTiles++;
	
	c = 25 * (PCTiles - OCTiles);
			
			
	//Corner Closeness
	
	int PCAlt = 0;
	int OCAlt = 0;
	double l = 0;
	
	if(positions[0][0] == ' ')   {
		if(positions[0][1] == player) PCAlt++;
		else if(positions[0][1] == other) OCAlt++;
		if(positions[1][1] == player) PCAlt++;
		else if(positions[1][1] == other) OCAlt++;
		if(positions[1][0] == player) PCAlt++;
		else if(positions[1][0] == other) OCAlt++;
	}
	if(positions[0][7] == ' ')   {
		if(positions[0][6] == player) PCAlt++;
		else if(positions[0][6] == other) OCAlt++;
		if(positions[1][6] == player) PCAlt++;
		else if(positions[1][6] == other) OCAlt++;
		if(positions[1][7] == player) PCAlt++;
		else if(positions[1][7] == other) OCAlt++;
	}
	if(positions[7][0] == ' ')   {
		if(positions[7][1] == player) PCAlt++;
		else if(positions[7][1] == other) OCAlt++;
		if(positions[6][1] == player) PCAlt++;
		else if(positions[6][1] == other) OCAlt++;
		if(positions[6][0] == player) PCAlt++;
		else if(positions[6][0] == other) OCAlt++;
	}
	if(positions[7][7] == ' ')   {
		if(positions[6][7] == player) PCAlt++;
		else if(positions[6][7] == other) OCAlt++;
		if(positions[6][6] == player) PCAlt++;
		else if(positions[6][6] == other) OCAlt++;
		if(positions[7][6] == player) PCAlt++;
		else if(positions[7][6] == other) OCAlt++;
	}
	l = -12.5 * (PCAlt - OCAlt);	
		
	//Mobility
	
		
		
	int PAvailable = numValidMoves(player, other);
	int OAvailable = numValidMoves(other, player);
	double m = 0;
	
	if(PAvailable > OAvailable)
		m = (100.0 * PAvailable)/(PAvailable + OAvailable);
	else if(PAvailable < OAvailable)
		m = -(100.0 * OAvailable)/(PAvailable + OAvailable);
	else m = 0;	


// final weighted score
	double score = (10 * p) + (801.724 * c) + (382.026 * l) + (78.922 * m) + (74.396 * f) + (10 * d);
	return score;
	
}
	
	public int numValidMoves(char player, char other)
	{
		int count = 0;
		
		for(int i = 0; i < boardSize; i++)
		{
			for(int j = 0; j < boardSize; j++)
			{
				if(isValidMove(i,j,player,other))
					count++;
					
			}
		}
		
		return count;		
	}
	
	public char[][] getPositions()
	{
		return positions;
	}
	
	public void setPositions(char[][] positions)
	{
		
		for (int i = 0; i < this.positions.length; i++)
    		for(int j = 0; j < this.positions[i].length; j++)
    			this.positions[i][j] = positions[i][j];
	}
	
	public int getTokensB()
	{
		return bTokens;
	}
	
	public void setTokensB(int bTokens)
	{
		this.bTokens = bTokens;
	}
	
	public int getTokensR()
	{
		return wTokens;
	}
	
	public void setTokensR(int wTokens)
	{
		this.wTokens = wTokens;
	}
	
	public int getBoardSize()
	{
		return boardSize;
	}
	
}